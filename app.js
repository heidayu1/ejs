const express = require('express');
const app = express();

// set the view engine to EJS
app.set('view engine', 'ejs')

// use res.render to load up an ejs view file

// home page
app.get('/', function(req, res) {
    app.render('pages/index');
});

// about page
app.get('/about', function(req, res) {
    app.render('pages/about')
});

app.listen(8080);
console.log('Server is listening on http://127.0.0.1:8080')